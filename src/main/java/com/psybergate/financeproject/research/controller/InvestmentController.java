package com.psybergate.financeproject.research.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.psybergate.financeproject.research.domain.ForecastEntryModifier;
import com.psybergate.financeproject.research.domain.Investment;
import com.psybergate.financeproject.research.domain.Money;
import com.psybergate.financeproject.research.model.CalculatedInvestment;
import com.psybergate.financeproject.research.model.ContributionEvent;
import com.psybergate.financeproject.research.model.NewInvestment;
import com.psybergate.financeproject.research.service.InvestmentService;

@ApplicationScoped
public class InvestmentController {

	@Inject
	private NewInvestment newInvestmentData;

	@Inject
	private InvestmentService investmentService;

	@Inject
	private CalculatedInvestment calculatedInvestment;

	public void generateForecast() {
		Money principal = getMoney(newInvestmentData.getPrincipal());
		double interestRate = getInterestRate(newInvestmentData.getInterestRate());
		Money monthlyContribution = getMoney(newInvestmentData.getMonthlyContribution());
		Map<Integer, ForecastEntryModifier> modifiers = getForecastModifiers();
		int term = newInvestmentData.getTerm();

		Investment investment = new Investment(principal, interestRate, monthlyContribution, term, modifiers);

		calculatedInvestment.setInvestment(investment);
	}

	private double getInterestRate(String rateAsText) {
		return Double.valueOf(rateAsText);
	}

	private Money getMoney(String textAmount) {
		double amount = Double.valueOf(textAmount);
		BigDecimal preciseAmount = BigDecimal.valueOf(amount);
		Money money = new Money(preciseAmount);

		return money;
	}

	private int getTerm(String termAsText) {
		return Integer.valueOf(termAsText);
	}

	private Map<Integer, ForecastEntryModifier> getForecastModifiers() {
		Map<Integer, ContributionEvent> events = newInvestmentData.getEvents();
		Map<Integer, ForecastEntryModifier> modifiers = new HashMap<Integer, ForecastEntryModifier>();

		for (Map.Entry<Integer, ContributionEvent> eventEntry : events.entrySet()) {
			ContributionEvent contributionEvent = eventEntry.getValue();
			int month = getTerm(contributionEvent.getMonth());
			double interestRate = getInterestRate(contributionEvent.getInterestRate());
			Money contribution = getMoney(contributionEvent.getContribution());
			boolean rateIsOnceOff = contributionEvent.interestRateIsOnceOff();
			boolean contributionIsOnceOff = contributionEvent.contributionIsOnceOff();

			ForecastEntryModifier modifier = new ForecastEntryModifier(month, interestRate, rateIsOnceOff, contribution,
					contributionIsOnceOff);
			modifiers.put(month, modifier);
		}

		return modifiers;
	}
}
