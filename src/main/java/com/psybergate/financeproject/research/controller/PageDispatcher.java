package com.psybergate.financeproject.research.controller;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Named;

@ApplicationScoped
@Named("dispatcher")
public class PageDispatcher {

	public static final String CONTROLLER_MAPPINGS = "mappings/controllermappings.properties";

	public static final String WEBPAGE_MAPPINGS = "mappings/webpagemappings.properties";

	/**
	 * Format: {pageType: [controller instance, Method instance that handles the
	 * request]}
	 */
	private Map<String, Object[]> controllers;

	/**
	 * Format: {pageType specified in xhtml: the next xhtml file to be rendered}
	 */
	private Map<String, String> webPages;

	@PostConstruct
	public void loadPageAndControllerMappings() {
		loadWebPageMappings();
		loadControllers();
	}

	public String showPage(String typeOfPage) {
		typeOfPage = typeOfPage.toLowerCase();

		Object controller = controllers.get(typeOfPage)[0];
		Method handler = (Method) controllers.get(typeOfPage)[1];

		try {
			handler.invoke(controller);
		}
		catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}

		return webPages.get(typeOfPage);
	}

	private void loadWebPageMappings() {
		Properties props = loadProperties(WEBPAGE_MAPPINGS);

		Map<String, String> webPages = new HashMap<String, String>();

		for (Map.Entry<Object, Object> webPageEntry : props.entrySet()) {
			String pageType = ((String) webPageEntry.getKey()).toLowerCase();
			String pageSourceFile = (String) webPageEntry.getValue();
			webPages.put(pageType, pageSourceFile);
		}

		this.webPages = webPages;
	}

	private void loadControllers() {
		try {
			Properties props = loadProperties(CONTROLLER_MAPPINGS);

			Map<String, Object[]> controllers = new HashMap<String, Object[]>();

			for (Map.Entry<Object, Object> controllerEntry : props.entrySet()) {
				String pageType = ((String) controllerEntry.getKey()).toLowerCase();
				String handlerQualifiedName = (String) controllerEntry.getValue();
				int methodDelimiterIndex = handlerQualifiedName.indexOf('#');
				String controllerName = handlerQualifiedName.substring(0, methodDelimiterIndex);
				String methodName = handlerQualifiedName.substring(methodDelimiterIndex + 1);

				Class<?> controllerClazz = Class.forName(controllerName);
				Object controller = CDI.current().select(controllerClazz).get();

				Method requestHandler = controllerClazz.getDeclaredMethod(methodName);
				requestHandler.setAccessible(true);
				controllers.put(pageType, new Object[] { controller, requestHandler });

				this.controllers = controllers;
			}
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private Properties loadProperties(String propertiesFile) {
		try {
			Properties props = new Properties();
			InputStream propsFileStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(propertiesFile);
			props.load(propsFileStream);
			propsFileStream.close();
			return props;
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
