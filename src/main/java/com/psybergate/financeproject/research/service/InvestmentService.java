package com.psybergate.financeproject.research.service;

import com.psybergate.financeproject.research.domain.Investment;

public interface InvestmentService {

	public void generateForecast(Investment investment);

}
