package com.psybergate.financeproject.research.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Forecast {

	private Money principal;

	private double interestRate;

	private Money monthlyContribution;

	private int term;

	private Map<Integer, ForecastEntryModifier> modifiers;

	private List<ForecastEntry> forecastEntries;

	public Forecast(Money principal, double interestRate, Money monthlyContribution, int term,
			Map<Integer, ForecastEntryModifier> modifiers) {
		super();
		this.principal = principal;
		this.interestRate = interestRate;
		this.monthlyContribution = monthlyContribution;
		this.term = term;
		this.forecastEntries = new ArrayList<ForecastEntry>();
		this.modifiers = modifiers;
	}

	private static Map<Integer, ForecastEntryModifier> createEmptyModifiers() {
		return new HashMap<Integer, ForecastEntryModifier>();
	}

	public void addForecastEntry(ForecastEntry forecastEntry) {
		forecastEntries.add(forecastEntry);
	}

	public Money getPrincipal() {
		return principal;
	}

	public void setPrincipal(Money principal) {
		this.principal = principal;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public Money getMonthlyContribution() {
		return monthlyContribution;
	}

	public void setMonthlyContribution(Money monthlyContribution) {
		this.monthlyContribution = monthlyContribution;
	}

	public int getTerm() {
		return term;
	}

	public void setTerm(int term) {
		this.term = term;
	}

	public Map<Integer, ForecastEntryModifier> getModifiers() {
		return modifiers;
	}

	public void setModifiers(Map<Integer, ForecastEntryModifier> modifiers) {
		this.modifiers = modifiers;
	}

	public List<ForecastEntry> getForecastEntries() {
		return forecastEntries;
	}

	public void setForecastEntries(List<ForecastEntry> forecastEntries) {
		this.forecastEntries = forecastEntries;
	}

}
