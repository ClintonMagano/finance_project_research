package com.psybergate.financeproject.research.domain;

import java.util.Map;

public class PropertyBond extends Investment {

	public PropertyBond(Money principal, double interestRate, Money monthlyContribution, int term,
			Map<Integer, ForecastEntryModifier> modifiers) {
		super(principal, interestRate, monthlyContribution, term, modifiers);
	}

	@Override
	protected Money getContributionForMonth(int currentMonth) {
		// All fees relating to property bonds can be account for here.
		Money contribution = super.getContributionForMonth(currentMonth);
		return contribution.multiply(-1);
	}
}
