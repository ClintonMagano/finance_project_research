package com.psybergate.financeproject.research.domain;

import java.math.BigDecimal;

/**
 * Immutable, basic representation of money.
 * 
 * @author Clinton
 *
 * @since 29 June 2019
 */
public class Money {

	public static final Money ZERO = new Money(BigDecimal.ZERO);

	private BigDecimal amount;

	public Money(BigDecimal amount) {
		super();
		this.amount = amount.setScale(2, BigDecimal.ROUND_CEILING);
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public Money multiply(double value) {
		return multiply(BigDecimal.valueOf(value));
	}

	public Money multiply(Money value) {
		return multiply(value.getAmount());
	}

	public Money multiply(BigDecimal value) {
		BigDecimal product = getAmount().multiply(value);
		return new Money(product);
	}

	public Money add(double amount) {
		return add(BigDecimal.valueOf(amount));
	}

	public Money add(Money monthlyContribution) {
		BigDecimal contribution = monthlyContribution.getAmount();
		return add(contribution);
	}

	public Money add(BigDecimal amount) {
		BigDecimal sum = getAmount().add(amount);
		return new Money(sum);
	}

	@Override
	public String toString() {
		return amount.toString();
	}

}
