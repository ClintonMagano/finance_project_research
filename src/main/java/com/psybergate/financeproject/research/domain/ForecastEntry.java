package com.psybergate.financeproject.research.domain;

public class ForecastEntry {

	private int month;

	private Money openingBalance;

	private double interestRate;

	private Money interestAmount;

	private Money monthlyContribution;

	private Money closingBalance;

	public ForecastEntry(int month, Money openingBalance, double interestRate, Money monthlyContribution) {
		super();
		setMonth(month);
		setOpeningBalance(openingBalance);
		setInterestRate(interestRate);
		setMonthlyContribution(monthlyContribution);
		calculateInterestAndBalance();
	}

	private void calculateInterestAndBalance() {
		Money interestAmount = getOpeningBalance().multiply(interestRate);
		Money balanceWithInterest = getOpeningBalance().add(interestAmount);
		Money closingBalance = balanceWithInterest.add(monthlyContribution);

		setInterestAmount(interestAmount);
		setClosingBalance(closingBalance);
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public Money getOpeningBalance() {
		return openingBalance;
	}

	public void setOpeningBalance(Money openingBalance) {
		this.openingBalance = openingBalance;
	}

	public Money getClosingBalance() {
		return closingBalance;
	}

	public void setClosingBalance(Money closingBalance) {
		this.closingBalance = closingBalance;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public Money getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(Money interestAmount) {
		this.interestAmount = interestAmount;
	}

	public Money getMonthlyContribution() {
		return monthlyContribution;
	}

	public void setMonthlyContribution(Money totalContribution) {
		this.monthlyContribution = totalContribution;
	}

}
