package com.psybergate.financeproject.research.domain;

public class ForecastEntryModifier {

	private int month;

	private double interestRate;

	private boolean interestRateIsOnceOff;

	private Money contribution;

	private boolean contributionIsOnceOff;

	public ForecastEntryModifier(int month, double interestRate, Money contribution) {
		super();
		this.month = month;
		this.interestRate = interestRate;
		this.contribution = contribution;
	}

	public ForecastEntryModifier(int month, double interestRate, boolean interestRateIsOnceOff, Money contribution,
			boolean contributionIsOnceOff) {
		super();
		this.month = month;
		this.interestRate = interestRate;
		this.interestRateIsOnceOff = interestRateIsOnceOff;
		this.contribution = contribution;
		this.contributionIsOnceOff = contributionIsOnceOff;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public Money getContribution() {
		return contribution;
	}

	public void setContribution(Money contribution) {
		this.contribution = contribution;
	}

	public boolean interestRateIsOnceOff() {
		return interestRateIsOnceOff;
	}

	public void setInterestRateIsOnceOff(boolean rateIsOnceOff) {
		this.interestRateIsOnceOff = rateIsOnceOff;
	}

	public boolean contributionIsOnceOff() {
		return contributionIsOnceOff;
	}

	public void setContributionIsOnceOff(boolean contributionIsOnceOff) {
		this.contributionIsOnceOff = contributionIsOnceOff;
	}

}
