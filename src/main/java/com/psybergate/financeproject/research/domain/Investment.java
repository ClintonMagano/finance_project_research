package com.psybergate.financeproject.research.domain;

import java.util.HashMap;
import java.util.Map;

public class Investment {

	private static final int DEFAULT_MODIFIER = 0;

	private Money principal;

	private double interestRate;

	private Money monthlyContribution;

	private int term;

	private Map<Integer, ForecastEntryModifier> modifiers;

	private Forecast forecast;

	public Investment(Money principal, double interestRate, Money monthlyContribution, int term,
			Map<Integer, ForecastEntryModifier> modifiers) {
		super();
		setPrincipal(principal);
		setInterestRate(interestRate);
		setMonthlyContribution(monthlyContribution);
		setTerm(term);
		setModifiers(modifiers);
		createAndSetDefaultModifier();
		setForecast(new Forecast(principal, interestRate, monthlyContribution, term, modifiers));
		generateForecast(modifiers);
	}

	public Money getPrincipal() {
		return principal;
	}

	public void setPrincipal(Money principal) {
		this.principal = principal;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public int getTerm() {
		return term;
	}

	public void setTerm(int term) {
		this.term = term;
	}

	public Money getMonthlyContribution() {
		return monthlyContribution;
	}

	public void setMonthlyContribution(Money monthlyContribution) {
		this.monthlyContribution = monthlyContribution;
	}

	public Map<Integer, ForecastEntryModifier> getModifiers() {
		return modifiers;
	}

	public void setModifiers(Map<Integer, ForecastEntryModifier> modifiers) {
		this.modifiers = modifiers;
	}

	public ForecastEntryModifier getDefaultModifier() {
		return getModifiers().get(DEFAULT_MODIFIER);
	}

	public Forecast getForecast() {
		return forecast;
	}

	public void setForecast(Forecast forecast) {
		this.forecast = forecast;
	}

	/**
	 * The modifier with key 0 in the map 'modifiers' is used to set the current
	 * interest rate and contribution while generating the forecast. The initial
	 * rate and contribution that forms part of the state of this object is never
	 * modified once set.
	 */
	private void createAndSetDefaultModifier() {
		Map<Integer, ForecastEntryModifier> modifiers;
		if (getModifiers().size() == 0) {
			modifiers = new HashMap<Integer, ForecastEntryModifier>();
		}
		else {
			modifiers = getModifiers();
		}

		ForecastEntryModifier modifier = new ForecastEntryModifier(DEFAULT_MODIFIER, getInterestRate(),
				getMonthlyContribution());
		modifiers.put(DEFAULT_MODIFIER, modifier);
		setModifiers(modifiers);
	}

	private void generateForecast(Map<Integer, ForecastEntryModifier> modifiers) {
		int currentMonth = 1;

		double decimalInterestRate = getDecimalInterestRateForMonth(currentMonth);
		Money monthlyContribution = getContributionForMonth(currentMonth);
		ForecastEntry firstForecastEntry = new ForecastEntry(currentMonth, principal, decimalInterestRate,
				monthlyContribution);

		getForecast().addForecastEntry(firstForecastEntry);

		ForecastEntry prevEntry = firstForecastEntry;
		for (currentMonth++; currentMonth <= getTerm(); currentMonth++) {
			Money prevClosingBalance = prevEntry.getClosingBalance();
			decimalInterestRate = getDecimalInterestRateForMonth(currentMonth);
			monthlyContribution = getContributionForMonth(currentMonth);
			ForecastEntry nextEntry = new ForecastEntry(currentMonth, prevClosingBalance, decimalInterestRate,
					monthlyContribution);

			getForecast().addForecastEntry(nextEntry);
			prevEntry = nextEntry;
		}

		setForecast(forecast);
	}

	private double getDecimalInterestRateForMonth(int currentMonth) {
		ForecastEntryModifier modifier;
		// Ensures subsequent months use the last interest rate specified.
		if (getModifiers().containsKey(currentMonth)) {
			modifier = getModifiers().get(currentMonth);
			if (!modifier.interestRateIsOnceOff()) {
				setDefaultInterestRate(modifier);
			}
		}
		else {
			modifier = getDefaultModifier();
		}

		double interestRate = modifier.getInterestRate();
		interestRate /= 100;
		interestRate /= 12;
		return interestRate;
	}

	/**
	 * All the fees should be gathered resolved in this method so that a final
	 * contribution amount is used. This contribution amount represents the net
	 * contribution for the month, whether positive or negative.
	 * 
	 * @param currentMonth The month in which specific contribution is sought.
	 * @param modifiers    List of monthly withdrawals/deposits for specific months.
	 * 
	 * @return Contribution for the month, which includes all expenses and donations
	 *         for the month.
	 */
	protected Money getContributionForMonth(int currentMonth) {
		// Perform all tax and bank fee calcs and anything else here.

		ForecastEntryModifier modifier;
		// Ensures subsequent months use the last contribution specified.
		if (getModifiers().containsKey(currentMonth)) {
			modifier = getModifiers().get(currentMonth);
			if (!modifier.contributionIsOnceOff()) {
				setDefaultContribution(modifier);
			}
		}
		else {
			modifier = getDefaultModifier();
		}

		return modifier.getContribution();
	}

	private void setDefaultInterestRate(ForecastEntryModifier modifier) {
		getDefaultModifier().setInterestRate(modifier.getInterestRate());
	}

	private void setDefaultContribution(ForecastEntryModifier modifier) {
		getDefaultModifier().setContribution(modifier.getContribution());
	}

}
