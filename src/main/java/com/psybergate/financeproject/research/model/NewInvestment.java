package com.psybergate.financeproject.research.model;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named("newInvestment")
public class NewInvestment {

	private String principal;

	private String interestRate;

	private String monthlyContribution;

	private int term;

	private Map<Integer, ContributionEvent> events = new HashMap<Integer, ContributionEvent>();

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public String getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(String interestRate) {
		this.interestRate = interestRate;
	}

	public String getMonthlyContribution() {
		return monthlyContribution;
	}

	public void setMonthlyContribution(String monthlyContribution) {
		this.monthlyContribution = monthlyContribution;
	}

	public int getTerm() {
		return term;
	}

	public void setTerm(int term) {
		this.term = term;
	}

	public Map<Integer, ContributionEvent> getEvents() {
		return events;
	}

	public void setEvents(Map<Integer, ContributionEvent> events) {
		this.events = events;
	}

	public ContributionEvent getEvent(int eventNumber) {
		if (getEvents().containsKey(eventNumber)) {
			return getEvents().get(eventNumber);
		}

		ContributionEvent newEvent = new ContributionEvent();
		getEvents().put(eventNumber, newEvent);
		return newEvent;
	}
}
