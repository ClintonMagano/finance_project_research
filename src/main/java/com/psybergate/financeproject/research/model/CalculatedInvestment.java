package com.psybergate.financeproject.research.model;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.psybergate.financeproject.research.domain.Forecast;
import com.psybergate.financeproject.research.domain.Investment;
import com.psybergate.financeproject.research.domain.Money;

@RequestScoped
@Named("investment")
public class CalculatedInvestment {

	private Investment investment;

	public Investment getInvestment() {
		return investment;
	}

	public void setInvestment(Investment investment) {
		this.investment = investment;
	}

	public Money getMonthlyContribution() {
		return investment.getMonthlyContribution();
	}

	public Forecast getForecast() {
		return investment.getForecast();
	}
}
