package com.psybergate.financeproject.research.model;

public class ContributionEvent {

	private String month;

	private String interestRate;

	private String contribution;

	private boolean interestRateIsOnceOff;

	private boolean contributionIsOnceOff;

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(String interestRate) {
		this.interestRate = interestRate;
	}

	public String getContribution() {
		return contribution;
	}

	public void setContribution(String contribution) {
		this.contribution = contribution;
	}

	public boolean interestRateIsOnceOff() {
		return interestRateIsOnceOff;
	}

	public boolean getInterestRateIsOnceOff() {
		return interestRateIsOnceOff;
	}

	public void setInterestRateIsOnceOff(boolean rateIsOnceOff) {
		this.interestRateIsOnceOff = rateIsOnceOff;
	}

	public boolean contributionIsOnceOff() {
		return contributionIsOnceOff;
	}

	public boolean getContributionIsOnceOff() {
		return contributionIsOnceOff;
	}

	public void setContributionIsOnceOff(boolean contributionIsOnceOff) {
		this.contributionIsOnceOff = contributionIsOnceOff;
	}

	@Override
	public String toString() {
		return "ContributionEvent [month=" + month + ", interestRate=" + interestRate + ", contribution=" + contribution
				+ ", interestRateIsOnceOff=" + interestRateIsOnceOff + ", contributionIsOnceOff=" + contributionIsOnceOff + "]";
	}

}
